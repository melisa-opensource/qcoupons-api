<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Providers\MessagesProvider;
use App\Providers\ArrayHelper;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('messages', function() {
            return new MessagesProvider();
        });
        $this->app->singleton('arrays', function() {
            return new ArrayHelper();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
