<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_products extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'score', 'url_image', 'description'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'score'=>'float'
    ];
    
}
