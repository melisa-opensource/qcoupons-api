<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{

    protected $fillable = [
        'name', 'slug', 'score', 'url_image', 'description'
    ];

    protected $casts = [
        'score'=>'float'
    ];
    
    public function scopeByIds($builder, $ids)
    {
        return $builder->whereIn('id', $ids);
    }
    
    public function getByIds($ids)
    {
        return $this->byIds($ids)->get();
    }
    
    public function scopePaging($builder)
    {
        return $builder->select([
            'id',
            'name',
            'slug',
            'score',
            'url_image',
        ]);
    }
    
}
