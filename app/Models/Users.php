<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Users extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;
    
    protected $fillable = [
        'name', 'email', 'password', 'score', 'is_admin'
    ];
    
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at'
    ];
    
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_admin' => 'boolean',
    ];
    
    public function scopeByEmail($builder, $email)
    {
        return $builder->where('email', $email);
    }
    
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    
    public function ranking()
    {
        return \DB::select(implode(' ', [
            'SELECT s.*, @rank := @rank + 1 rank FROM (',
                'SELECT',
                    'id, name, email, score',
                'FROM users',
                'GROUP BY id, name, email, score',
            ') s,', 
            '(SELECT @rank := 0) init',
            'ORDER BY score DESC'
        ]));
    }
    
}
