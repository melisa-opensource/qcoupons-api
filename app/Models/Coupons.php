<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'score', 'user_id'
    ];
    
    public function scopeByCode($builder, $code)
    {
        return $builder->where('code', $code);
    }
    
    public function getByCode($code)
    {
        return $this->byCode($code)->first();
    }
    
}
