<?php

namespace App\Http\Requests;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class PagingRequest extends FormRequest
{
    
    public function rules()
    {
        return [
            'page'=>'required|integer',
            'limit'=>'required|integer'
        ];
    }
    
}
