<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest as Request;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class FormRequest extends Request
{
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function allValid()
    {
        return $this->only(array_keys($this->rules()));
    }
    
    public function getErrorCode()
    {
        return $this->errorCode;
    }
    
    public function withValidator($validator)
    {
        $validator->errorCode = $this->getErrorCode();
    }
    
    public function rules()
    {
        return $this->rules;
    }
    
}
