<?php

namespace App\Http\Requests\Products;

use App\Http\Requests\PagingRequest as Request;

class PagingRequest extends Request
{
    
    protected $errorCode = [
        'page'=>'products.page',
        'limit'=>'products.limit',
    ];
    
}
