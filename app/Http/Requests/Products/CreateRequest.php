<?php

namespace App\Http\Requests\Products;

use App\Http\Requests\FormRequest as Request;

class CreateRequest extends Request
{
    
    protected $rules = [
        'name'=>'required|max:200|unique:products',
        'score'=>'required|integer',
        'url_image'=>'required|url|max:250',
        'description'=>'sometimes|max:250',
    ];
    
    protected $errorCode = [
        'name'=>'products.create.fr.name',
        'score'=>'products.create.fr.score',
        'url_image'=>'products.create.fr.url_image',
        'description'=>'products.create.fr.description',
    ];
    
}
