<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\ReportRequest as FormRequest;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class ReportRequest extends FormRequest
{
    
    protected $rules = [
        'id'=>'required|integer|exists:users',
    ];

    protected $errorCode = [
        'id'=>'users.report.fr.id',
    ];
    
}
