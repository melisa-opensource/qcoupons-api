<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\FormRequest;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class RegisterRequest extends FormRequest
{
    
    protected $rules = [
        'name'=>'required',
        'email'=>'required|email|unique:users',
        'password'=>'required|min:6',
    ];

    protected $errorCode = [
        'name'=>'users.register.fr.name',
        'email'=>'users.register.fr.email',
        'password'=>'users.register.fr.password',
    ];
    
}
