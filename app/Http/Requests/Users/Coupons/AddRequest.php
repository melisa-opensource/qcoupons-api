<?php

namespace App\Http\Requests\Users\Coupons;

use App\Http\Requests\FormRequest;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class AddRequest extends FormRequest
{
    
    protected $rules = [
        'code'=>'required|max:6|exists:coupons,code'
    ];
    
    protected $errorCode = [
        'code'=>'users.coupons.add.fr.code'
    ];
    
}
