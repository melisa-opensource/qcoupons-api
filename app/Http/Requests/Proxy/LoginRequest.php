<?php

namespace App\Http\Requests\Proxy;

use App\Http\Requests\FormRequest;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class LoginRequest extends FormRequest
{
    
    protected $rules = [
        'email'=>'required|email',
        'password'=>'required',
        'client_id'=>'required',
    ];
    
    protected $errorCode = [
        'email'=>'security.login.email',
        'password'=>'security.login.password',
        'client_id'=>'security.client_id.required',
    ];
    
    public function validationData()
    {
        $client_id = $this->header('client_id');
        $this->merge([
            'client_id'=>$client_id ? $client_id : 2
        ]);
        return parent::validationData();
    }
    
}
