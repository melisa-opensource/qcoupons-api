<?php

namespace App\Http\Requests\Shopping;

use App\Http\Requests\FormRequest as Request;

class CreateRequest extends Request
{
    
    protected $rules = [
        'ids'=>'required|array|min:1',
    ];
    
    protected $errorCode = [
        'ids'=>'shopping.create.fr.ids',
    ];
    
    public function validationData()
    {
        $inputIds = $this->get('ids');
        if (!is_array($inputIds)) {
            $inputIds = json_decode($inputIds, true);
        }
        $this->merge([
            'ids'=>$inputIds
        ]);
        return parent::validationData();
    }
    
}
