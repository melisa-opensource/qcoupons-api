<?php

namespace App\Http\Requests;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class ReportRequest extends FormRequest
{
    
    protected $rules = [
        'id'=>'required|integer'
    ];

    public function rules()
    {
        return $this->rules;
    }
    
    public function validationData()
    {
        $this->merge([
            'id'=>$this->route('id')
        ]);
        return parent::validationData();
    }
    
}
