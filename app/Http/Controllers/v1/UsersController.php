<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Logics\Users\RankingLogic;
use App\Logics\Users\RegisterLogic;
use App\Logics\Users\ReportLogic;
use App\Http\Requests\PagingRequest;
use App\Http\Requests\Users\RegisterRequest;
use App\Http\Requests\Users\ReportRequest;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class UsersController extends Controller
{
    
    public function register(
        RegisterRequest $request,
        RegisterLogic $logic
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input);
        return response()->create($result);
    }
    
    public function ranking(
        PagingRequest $request,
        RankingLogic $logic
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input);
        return response()->paging($result);
    }
    
    public function report(
        ReportRequest $request,
        ReportLogic $logic
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input['id']);
        return response()->data($result);
    }
    
    public function profile(
        ReportLogic $logic
    )
    {
        $id = auth()->user()->id; 
        $result = $logic->init($id);
        return response()->data($result);
    }
    
}
