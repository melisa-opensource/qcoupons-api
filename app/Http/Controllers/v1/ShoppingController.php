<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shopping\CreateRequest;
use App\Logics\Shopping\CreateLogic;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class ShoppingController extends Controller
{
    
    public function create(
        CreateRequest $request,
        CreateLogic $logic
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input);
        return response()->create($result);
    }
    
}
