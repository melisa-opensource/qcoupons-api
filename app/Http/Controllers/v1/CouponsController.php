<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Coupons\PagingRequest;
use App\Logics\Coupons\PagingLogic;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class CouponsController extends Controller
{
    
    public function paging(
        PagingLogic $logic,
        PagingRequest $request
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input);
        return response()->paging($result);
    }
    
}
