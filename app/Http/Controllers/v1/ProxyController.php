<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Logics\Proxy\LoginLogic;
use App\Logics\Proxy\LogoutLogic;
use App\Http\Requests\Proxy\LoginRequest;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class ProxyController extends Controller
{
    
    public function login(
        LoginLogic $logic,
        LoginRequest $request
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input);
        return response()->data($result);
    }
    
    public function logout(
        LogoutLogic $logic
    )
    {
        $result = $logic->init();
        return response()->data($result);
    }
    
}
