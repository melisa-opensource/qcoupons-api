<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Products\PagingRequest;
use App\Http\Requests\ReportRequest;
use App\Http\Requests\Products\CreateRequest;
use App\Logics\Products\PagingLogic;
use App\Logics\Products\ReportLogic;
use App\Logics\Products\CreateLogic;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class ProductsController extends Controller
{
    
    public function create(
        CreateRequest $request,
        CreateLogic $logic
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input);
        return response()->create($result);
    }
    
    public function report(
        ReportRequest $request,
        ReportLogic $logic
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input['id']);
        return response()->data($result);
    }
    
    public function paging(
        PagingLogic $logic,
        PagingRequest $request
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input);
        return response()->paging($result);
    }
    
}
