<?php

namespace App\Http\Controllers\v1\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\Coupons\AddRequest;
use App\Logics\Users\Coupons\AddLogic;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class CouponsController extends Controller
{
    
    public function add(
        AddRequest $request,
        AddLogic $logic
    )
    {
        $input = $request->allValid();
        $result = $logic->init($input);
        return response()->create($result);
    }
    
}
