<?php

namespace App\Http\Middleware;

use Closure;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class Cors
{
    
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (!method_exists($response, 'header')) {
            return $response;
        }
        return $response->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Headers', 'access-control-allow-origin, authorization, Origin, X-Requested-With, Content-Type, Accept, client_id')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }
    
}
