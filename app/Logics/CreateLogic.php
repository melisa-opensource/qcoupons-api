<?php

namespace App\Logics;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class CreateLogic
{
    use LogicBusiness;
    
    protected $repository;
    protected $fireEvent;

    public function __construct($repository)
    {
        $this->repository = $repository;
    }
    
    public function init(array $input)
    {
        $this->initTransaction($this->repository);
        $this->injectUserId($input);
        $result = $this->save($input);
        if (!$result) {
            return $this->rollBack($this->repository);
        }
        $event = $this->generateEvent($input, $result);        
        if (!$this->fireEvent($event)) {
            return $this->repository->rollBack();
        }        
        $this->commit($this->repository);
        return $this->getReturnData($result, $event, $input);
    }
    
    public function getReturnData($result, $event, $input)
    {
        return $result;
    }
    
    public function fireEvent(array $event)
    {        
        $emitEvent = $this->getFireEvent();        
        if (!$emitEvent) {
            return true;
        }        
        return $this->emitEvent($emitEvent, $event);   
    }
    
    public function getFireEvent()
    {
        return $this->fireEvent;
    }
    
    public function generateEvent($input, $result)
    {
        return [
            'id'=>$result->id,
        ];
    }
    
    public function injectUserId(&$input)
    {
        $input ['user_id']= $this->getUserId();
    }
    
    public function save(&$input)
    {
        $result = $this->repository->create($input);        
        if ($result) {
            return $result;
        }
        return false;
    }
    
}
