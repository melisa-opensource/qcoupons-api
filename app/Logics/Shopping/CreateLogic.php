<?php

namespace App\Logics\Shopping;

use App\Logics\CreateLogic as Logic;
use App\Models\Products;
use App\Models\User_products;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class CreateLogic extends Logic
{
    
    protected $repoProducts;

    public function __construct(
        Products $repoProducts,
        User_products $repository
    )
    {
        $this->repoProducts = $repoProducts;
        parent::__construct($repository);
    }
    
    public function save(&$input)
    {
        $products = $this->getProducts($input['ids']);
        if (!$products) {
            return false;
        }
        $user = $this->getUser();
        if (!$user) {
            return false;
        }
        if (!$this->isValidProducts($products, $user)) {
            return false;
        }
        if (!$this->createShopping($products, $user->id)) {
            return false;
        }
        if (!$this->updateUserScore($user, $products->sum('score'))) {
            return false;
        }
        return $user;
    }
    
    public function updateUserScore(&$user, $scoreDiscount)
    {
        $user->score = $user->score - $scoreDiscount;
        if ($user->save()) {
            return true;
        }
        return $this->errorCode('shopping.create.user.update');
    }
    
    public function getTotalProductScore(&$products)
    {
        return $products->sum('score');
    }
    
    public function createShopping(&$products, $idUser)
    {
        $userProducts = $products->map(function ($product) use ($idUser) {
            return [
                'user_id'=>$idUser,
                'product_id'=>$product->id
            ];
        })->all();
        return $this->repository->insert($userProducts);
    }
    
    public function isValidProducts(&$products, &$user)
    {
        if (!$products->count()) {
            return $this->errorCode('shopping.create.empty');
        }
        $totalScore = $products->sum('score');
        if ($totalScore <= $user->score) {
            return true;
        }
        return $this->errorCode('shopping.create.points');
    }
    
    public function getProducts($ids)
    {
        $result = $this->repoProducts->getByIds($ids);
        if ($result) {
            return $result;
        }
        return $this->errorCode('shopping.create.products');
    }
    
}
