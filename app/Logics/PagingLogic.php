<?php

namespace App\Logics;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class PagingLogic
{
    
    public function init(array $input)
    {
        if (!isset($input['limit'])) {
            $input ['limit']= 15;
        }        
        $result = $this->runQuery($input);        
        if (!$result) {
            return false;
        }        
        if ($result->total() === 0) {            
            return $this->returnDataEmpty();            
        }        
        return $this->returnData($result, $input);
    }
    
    public function returnDataEmpty()
    {
        return [
            'total'=>0,
            'data'=>[],
        ];
    }
    
    public function returnData(&$result, &$input)
    {
        return [
            'total'=>$result->total(),
            'data'=>$result->toArray()['data']
        ];
    }
    
    public function runQuery(&$input)
    {        
        $result = $this->repository->paginate((int)$input['limit']);       
        return $result;
    }
    
}
