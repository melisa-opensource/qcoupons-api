<?php

namespace App\Logics\Proxy;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;
use App\Logics\LogicBusiness;
use App\Models\Users;
use App\Models\OAuthClients;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class LoginLogic
{
    use LogicBusiness;
    
    protected $repoUsers;
    protected $repoOauthClients;

    public function __construct(
        Users $repoUsers,
        OAuthClients $repoOauthClients
    )
    {
        $this->repoUsers = $repoUsers;
        $this->repoOauthClients = $repoOauthClients;
    }
    
    public function init(array $input)
    {
        $user = $this->getUser($input['email']);      
        if( !$user) {
            return false;
        }        
        $client = $this->getClient($input['client_id']);
        if( !$client) {
            return false;
        }
        return $this->proxyLogin('password', $user, $client, $input['password']);
    }
    
    public function getClient($id)
    {
        $result = $this->repoOauthClients->getById($id);        
        if( $result) {
            return $result;
        }        
        return $this->errorCode('security.login.client.notfound');
    }
    
    public function proxyLogin($grantType, $user, $client, $password)
    {
        $params = array_merge([
            'username'=>$user->email,
            'password'=>$password,
        ], [
            'client_id'=>$client->id,
            'client_secret'=>$client->secret,
            'grant_type'=>$grantType
        ]);
        try {
            $http = new Client([
                'base_uri'=>env('APP_URL')
            ]);
            $response = $http->post('oauth/token', [
                'form_params'=>$params
            ]);
        } catch (ServerException $ex) {
            return $this->errorCode('security.login.serverException', [
                'message'=>$ex->getMessage()
            ]);
        } catch (ClientException $ex) {
            return $this->errorCode('security.login.clientException', [
                'message'=>$ex->getMessage()
            ]);
        }
        if ($response->getStatusCode() !== 200) {
            return $this->errorCode('security.login.statusCode.invalid');
        }        
        $result = json_decode($response->getBody()->getContents());
        if ( is_null($result) || !isset($result->access_token)) {
            return $this->errorCode('security.login.reponse.invalid');
        }
        return [
            'access_token'=>$result->access_token,
            'expires_in'=>$result->expires_in,
            'refresh_token'=>$result->refresh_token,
            'user'=>$user->only([
                'id',
                'name',
                'email',
                'score',
                'is_admin',
            ])
        ];
    }
    
    public function getUser($email)
    {
        $user = $this->repoUsers
            ->byEmail($email)
            ->first();        
        if (!is_null($user)) {
            return $user;
        }        
        return $this->errorCode('security.login.email.notfound', [
            'email'=>$email
        ]);
    }
    
}
