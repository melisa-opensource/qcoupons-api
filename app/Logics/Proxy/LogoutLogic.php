<?php

namespace App\Logics\Proxy;

use App\Logics\LogicBusiness;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class LogoutLogic
{
    use LogicBusiness;
    
    /**
     * Logs out the user. We revoke access token and refresh token. 
     * Also instruct the client to forget the refresh cookie.
     */
    public function init()
    {
        $user = $this->getUser();
        if (!$user) {
            return false;
        }
        $accessToken = $this->getAccessToken($user);
        if (!$accessToken) {
            return false;
        }
        $this->revokeAccessToken($accessToken);
        $event = [
            'id'=>$user->id,
        ];
        if( !$this->emitEvent('security.users.logout.success', $event)) {
            return false;
        }
        return true;
    }
    
    public function revokeAccessToken($accessToken)
    {
        \DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked'=>true
            ]);        
        $accessToken->revoke();
        return true;
    }
    
    public function getAccessToken(&$user)
    {
        $accessToken = $user->token();        
        if ($accessToken) {
            return $accessToken;
        }
        return $this->errorCode('security.users.logout.token');
    }
    
    public function getUser()
    {
        $user = app('auth')->user();        
        if ($user) {
            return $user;
        }
        return $this->errorCode('security.users.logout.user');
    }
    
}
