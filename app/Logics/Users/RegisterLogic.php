<?php

namespace App\Logics\Users;

use App\Models\Users;
use App\Logics\LogicBusiness;
use App\Logics\Proxy\LoginLogic;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class RegisterLogic
{
    use LogicBusiness;
    
    protected $repoUsers;
    protected $loginLogic;

    public function __construct(
        Users $repoUsers,
        LoginLogic $loginLogic
    )
    {
        $this->repoUsers = $repoUsers;
        $this->loginLogic = $loginLogic;
    }
    
    public function init(array $input)
    {
        $user = $this->createUser($input);
        if (!$user) {
            return false;
        }
        $result = $this->autoLogin($user->email, $input['password']);
        if (!$result) {
            return false;
        }
        if (!$this->fireEvent($user)) {
            return false;
        }
        return $result;
    }
    
    public function autoLogin($email, $passord)
    {
        return $this->loginLogic->init([
            'email'=>$email,
            'password'=>$passord,
            'client_id'=>2
        ]);
    }
    
    /**
     * Send email, notification slack in queue process
     * @param type $user
     * @return boolean
     */
    public function fireEvent(&$user)
    {
        $event = [
            'id'=>$user->id
        ];
        return $this->emitEvent('users.register.success', $event);
    }
    
    public function createUser(&$input)
    {
        $result = $this->repoUsers->create($input);
        if ($result) {
            return $result;
        }
        return $this->errorCode('users.register.create');
    }
    
}
