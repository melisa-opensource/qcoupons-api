<?php

namespace App\Logics\Users;

use App\Models\Users;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class RankingLogic
{
    
    public function __construct(Users $repo)
    {
        $this->repository = $repo;
    }
    
    public function init(array $input)
    {
        return $this->runQuery($input);
    }
    
    public function runQuery(&$input)
    {        
        $result = $this->repository->ranking($input);
        return [
            'data'=>$result,
            'total'=>count($result)
        ];
    }
    
}