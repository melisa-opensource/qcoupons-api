<?php

namespace App\Logics\Users\Coupons;

use App\Logics\CreateLogic;
use App\Models\Coupons;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class AddLogic extends CreateLogic
{
    
    public function __construct(Coupons $repository)
    {
        parent::__construct($repository);
    }
    
    public function save(&$input)
    {
        $coupon = $this->getCoupon($input['code']);
        if (!$coupon) {
            return false;
        }
        if (!$this->isValidCoupon($coupon)) {
            return false;
        }
        if (!$this->updateCoupon($coupon, $input['user_id'])) {
            return false;
        }
        $user = $this->getUserWithNewScore($coupon->score);
        if (!$user) {
            return false;
        }
        if (!$this->updateUserScore($user)) {
            return false;
        }
        return $user;
    }
    
    public function getUserWithNewScore($score)
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->errorCode('users.coupos.add.user');
        }
        $user->score = $user->score + $score;
        return $user;
    }
    
    public function updateUserScore(&$user)
    {        
        if ($user->save()) {
            return $user;
        }
        return $this->errorCode('users.coupons.add.update.user');
    }
    
    public function updateCoupon(&$coupon, $idUser)
    {
        $coupon->user_id = $idUser;
        if ($coupon->save()) {
            return true;
        }
        return $this->errorCode('users.coupons.add.update.coupon.update');
    }
    
    public function isValidCoupon(&$coupon)
    {
        if (!$coupon->user_id) {
            return true;
        }
        return $this->errorCode('users.coupons.add.coupon.redeemed');
    }
    
    public function getCoupon($code)
    {
        $result = $this->repository->getByCode($code);
        if ($result) {
            return $result;
        }
        return $this->errorCode('users.coupons.add.coupon.notFound');
    }
    
}
