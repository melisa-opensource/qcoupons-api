<?php

namespace App\Logics\Users;

use App\Logics\ReportLogic as Logic;
use App\Models\Users;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class ReportLogic extends Logic
{

    public function __construct(Users $repo)
    {
        parent::__construct($repo);
    }
    
    public function getRecord($id)
    {
        $user = $this->getUser();
        if ($user->is_admin) {
            return parent::getRecord($id);
        }
        if ($user->id === $id) {
            return parent::getRecord($id);
        }
        return $this->errorCode('users.report.notAllowed');
    }
    
}
