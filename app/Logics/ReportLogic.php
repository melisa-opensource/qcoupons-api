<?php

namespace App\Logics;

use App\Logics\LogicBusiness;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class ReportLogic
{
    use LogicBusiness;
    
    protected $repository;
    protected $errorCode;

    public function __construct($repo)
    {
        $this->repository = $repo;
    }
    
    public function init($id)
    {
        $record = $this->getRecord($id);
        if ($record) {
            return $record;
        }
        return $this->errorCode($this->errorCode);
    }
    
    public function getRecord($id)
    {
        return $this->repository->find($id);
    }
    
}
