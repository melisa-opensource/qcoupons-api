<?php

namespace App\Logics\Products;

use App\Logics\ReportLogic as Logic;
use App\Models\Products;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class ReportLogic extends Logic
{
    
    protected $errorCode = 'products.report.notFound';

    public function __construct(Products $repo)
    {
        parent::__construct($repo);
    }
    
}
