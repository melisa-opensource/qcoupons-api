<?php

namespace App\Logics\Products;

use App\Logics\PagingLogic as Logic;
use App\Models\Products;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class PagingLogic extends Logic
{
    
    public function __construct(Products $repo)
    {
        $this->repository = $repo;
    }
    
    public function runQuery(&$input)
    {        
        $result = $this->repository
            ->paging()
            ->orderBy('created_at', 'desc')
            ->paginate((int)$input['limit']);       
        return $result;
    }
    
}
