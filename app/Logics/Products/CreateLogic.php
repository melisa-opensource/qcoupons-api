<?php

namespace App\Logics\Products;

use Illuminate\Support\Str;
use App\Logics\CreateLogic as Logic;
use App\Models\Products;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class CreateLogic extends Logic
{
    
    public function __construct(Products $repository)
    {
        parent::__construct($repository);
    }
    
    public function save(&$input)
    {
        if (!$this->isAllow('products.create.notAllowed')) {
            return false;
        }
        $input ['slug']= Str::slug($input['name']);
        return parent::save($input);
    }
    
}
