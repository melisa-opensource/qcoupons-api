<?php

namespace App\Logics\Coupons;

use App\Logics\PagingLogic as Logic;
use App\Models\Coupons;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
class PagingLogic extends Logic
{
    
    public function __construct(Coupons $repo)
    {
        $this->repository = $repo;
    }
    
}
