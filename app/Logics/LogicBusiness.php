<?php

namespace App\Logics;

/**
 * 
 *
 * @author Luis Josafat Heredia Contreras
 */
trait LogicBusiness
{
    
    /**
     * En un escenario real esta función funciona como caja negra y comprueba
     * la clave de una tarea ejecutando un proceso de validación RBAC y/o ABAC
     * 
     * Se mantiene una simple comprobación de atributo ABAC establecida en la entidad
     * usuarios
     * @param string $errorCode
     * @return boolean
     */
    public function isAllow($errorCode)
    {
        $user = $this->getUser();
        if (!$user) {
            return false;
        }
        if ($user->is_admin) {
            return true;
        }
        return $this->errorCode($errorCode);
    }
    
    public function getUser()
    {
        return auth()->user();
    }
    
    public function getUserId()
    {
        $user = $this->getUser();
        return $user ? $user->id : null;
    }
    
    // normally this method goes in the repository class
    public function initTransaction($repo)
    {
        $repo->getConnection()->beginTransaction();
    }
    
    // normally this method goes in the repository class
    public function rollBack($repo)
    {
        $repo->getConnection()->rollBack();
        return false;
    }
    
    // normally this method goes in the repository class
    public function commit($repo)
    {
        $repo->getConnection()->commit();
        return true;
    }
    
    public function errorCode($code, array $data = [])
    {        
        return app('messages')->errorCode($code, $data);        
    }
    
    /**
     * Register event to binnacle
     * @param string $key
     * @param array $data
     * @return boolean
     */
    public function emitEvent($key, $data = null)
    {
        // logic event
        return true;
    }
    
}
