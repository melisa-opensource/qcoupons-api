<?php

use Illuminate\Database\Seeder;
use App\Models\Coupons;

/**
 * Install coupons
 *
 * @author Luis Josafat Heredia Contreras
 */
class CouponsSeeder extends Seeder
{
    
    public function run()
    {
        factory(Coupons::class, 25)
            ->create();
    }   
    
}
