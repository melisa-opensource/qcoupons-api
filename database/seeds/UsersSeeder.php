<?php

use Illuminate\Database\Seeder;
use App\Models\Users;

/**
 * Install users
 *
 * @author Luis Josafat Heredia Contreras
 */
class UsersSeeder extends Seeder
{
    
    public function run()
    {
        $this->userDeveloper();
        $this->userNoAdmin();
        $this->fakeUsers();
    }
    
    public function fakeUsers()
    {
        factory(Users::class, 20)->create();
    }
    
    public function userNoAdmin()
    {
        $name = env('USER_NO_ADMIN_NAME');
        $password = env('USER_NO_ADMIN_PASSWORD');
        $email = env('USER_NO_ADMIN_EMAIL');
        $this->installUser($name, [
            'email'=>$email,
            'is_admin'=>false,
            'password'=>$password
        ]);
    }
    
    public function userDeveloper()
    {
        $password = env('USER_DEVELOPER_PASSWORD', 'developer');
        $email = env('USER_DEVELOPER_EMAIL');
        $this->installUser('developer', [
            'email'=>$email,
            'is_admin'=>true,
            'score'=>1000,
            'password'=>$password
        ]);
    }
    
    public function installUser($name, $values)
    {
        return Users::updateOrCreate([
            'name'=>$name
        ], $values); 
    }
    
}
