<?php

use Illuminate\Database\Seeder;
use App\Models\Products;

/**
 * Install products
 *
 * @author Luis Josafat Heredia Contreras
 */
class ProductsSeeder extends Seeder
{
    
    public function run()
    {
        factory(Products::class, 25)
            ->create();
    }   
    
}
