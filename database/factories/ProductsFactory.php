<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Products;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Products::class, function (Faker $faker) {
    $name = $faker->name;
    $slug = Str::slug($name);
    $description = $faker->text;
    $score = $faker->numberBetween(100, 7000);
    $urlImage = $faker->randomElement([
        'https://placeimg.com/640/480/animals',
        'https://placeimg.com/640/480/arch',
        'https://placeimg.com/640/480/nature',
        'https://placeimg.com/640/480/people',
        'https://placeimg.com/640/480/tech',
        'https://placeimg.com/640/480/any'
    ]);
    return [
        'name'=>$name,
        'slug'=>$slug,
        'description'=>$description,
        'score'=>$score,
        'url_image'=>$urlImage
    ];
});
