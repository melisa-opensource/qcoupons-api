<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Coupons;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Coupons::class, function (Faker $faker) {
    $code = $faker->bothify('###???');
    $score = $faker->numberBetween(100, 7000);
    return [
        'code'=>$code,
        'score'=>$score
    ];
});
