<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Models\Users;
use Tests\ResponseTrait;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use ResponseTrait;
    
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = $this->findUser();
    }    
    
    public function findUser($key = 'developer')
    {
        return Users::where('name', $key)->firstOrFail();
    }
    
    public function simpleApiPost($endpoint, $input = [])
    {
        return $this->json('post', $endpoint, $input);
    }
    
    public function simpleApiGet($endpoint, $input = [])
    {
        $inputGet = http_build_query($input);
        $url = implode('', [
            $endpoint,
            '?',
            $inputGet
        ]);
        return $this->json('get', $url);
    }    
    
    public function apiGet($endpoint, $input = [], $user = null)
    {
        $inputGet = http_build_query($input);
        $url = implode('', [
            $endpoint,
            '?',
            $inputGet
        ]);
        return $this
            ->actingAs($user ? $user : $this->user, 'api')
            ->json('get', $url);
    }
    
    public function apiPost($endpoint, $input = [], $user = null)
    {
        return $this
            ->actingAs($user ? $user : $this->user, 'api')
            ->json('post', $endpoint, $input);
    }
    
}
