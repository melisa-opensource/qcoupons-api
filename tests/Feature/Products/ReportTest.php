<?php

namespace Tests\Feature\Products;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\Products;

class ReportTest extends TestCase
{
    use DatabaseTransactions;
    
    protected $endpoint = '/api/v1/products';
    
    /**
     * @group products
     * @group products.report
     * @group reports
     * @group completed
     * @test
     */
    public function success()
    {
        $product = factory(Products::class)->create();
        $url = "$this->endpoint/$product->id";
        $response = $this->simpleApiGet($url);
        $this->responseSuccess($response);
        $this->assertDatabaseHas('products', [
            'id'=>$product->id
        ]);
    }
    
    /**
     * @group products
     * @group products.report
     * @group reports
     * @group completed
     * @test
     */
    public function error_not_found()
    {
        $id = time();
        $url = "$this->endpoint/$id";
        $response = $this->simpleApiGet($url);
        $this->responseWithErrors($response);
        $this->responseWithError($response, 'products.report.notFound');
        $this->assertDatabaseMissing('products', [
            'id'=>$id
        ]);
    }
    
}
