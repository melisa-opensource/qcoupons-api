<?php

namespace Tests\Feature;

use Tests\TestCase;

class PagingTest extends TestCase
{
    
    protected $endpoint = '/api/v1/products';
    
    /**
     * @group products
     * @group products.paging
     * @group completed
     * @test
     */
    public function paging()
    {
        $response = $this->simpleApiGet($this->endpoint, [
            'limit'=>25,
            'page'=>1
        ]);
        $this->responsePagingSuccess($response);
    }
    
    /**
     * @group products
     * @group products.paging
     * @group completed
     * @test
     */
    public function errors_input_required()
    {
        $response = $this->simpleApiGet($this->endpoint);
        $this->responseWithErrors($response);
    }
    
}
