<?php

namespace Tests\Feature\Products;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\Products;

class CreateTest extends TestCase
{
    use DatabaseTransactions;
    
    protected $endpoint = '/api/v1/products';
    
    /**
     * @group products
     * @group products.create
     * @group create
     * @group completed
     * @test
     */
    public function success()
    {
        $input = factory(Products::class)->make();
        $response = $this->apiPost($this->endpoint, $input->toArray());
        $this->responseCreatedSuccess($response);
        $result = json_decode($response->content());
        $this->assertDatabaseHas('products', [
            'id'=>$result->data->id,
            'name'=>$input->name,
            'slug'=>Str::slug($input->name)
        ]);
    }
    
    /**
     * @group products
     * @group products.create
     * @group create
     * @group completed
     * @test
     */
    public function input_required()
    {
        $input = [];
        $response = $this->apiPost($this->endpoint, $input);
        $this->responseWithError($response, 'products.create.fr.name');
        $this->responseWithError($response, 'products.create.fr.score');
        $this->responseWithError($response, 'products.create.fr.url_image');
    }
    
    /**
     * @group products
     * @group products.create
     * @group create
     * @group completed
     * @test
     */
    public function not_authenticated()
    {
        $response = $this->simpleApiPost($this->endpoint);
        $this->responseWithError($response, 'security.auth.unauthenticated');
    }
    
}
