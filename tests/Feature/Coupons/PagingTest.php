<?php

namespace Tests\Feature\Coupons;

use Tests\TestCase;

class PagingTest extends TestCase
{
    
    protected $endpoint = '/api/v1/coupons';
    
    /**
     * @group coupons
     * @group coupons.paging
     * @group completed
     * @test
     */
    public function success()
    {
        $response = $this->apiGet($this->endpoint, [
            'limit'=>25,
            'page'=>1
        ]);
        $this->responsePagingSuccess($response);
    }
    
    
    /**
     * @group coupons
     * @group coupons.paging
     * @group completed
     * @test
     */
    public function errors_input_required()
    {
        $response = $this->apiGet($this->endpoint);
        $this->responseWithErrors($response);
    }
    
}
