<?php

namespace Tests\Feature\Users;

use Faker\Generator as Faker;
use Tests\TestCase;
use App\Models\Users;

class CreateTest extends TestCase
{
    
    protected $endpoint = '/api/v1/register';
    
    /**
     * @group users
     * @group users.register
     * @group create
     * @group completed
     * @test
     */
    public function success()
    {
        $input = factory(Users::class)->make()->toArray();
        $password = app(Faker::class)->password;
        $input ['email']= $input['email'] . time();
        $input ['password']= $password;
        $response = $this->simpleApiPost($this->endpoint, $input);
        $this->responseSuccess($response);
        $result = json_decode($response->content());
        $this->assertTrue(isset($result->data->access_token));
        $this->assertTrue(isset($result->data->refresh_token));
        $this->assertDatabaseHas('users', [
            'id'=>$result->data->user->id,
            'name'=>$input['name'],
            'email'=>$input['email'],
            'is_admin'=>false
        ]);
    }
    
    /**
     * @group users
     * @group users.register
     * @group create
     * @group completed
     * @test
     */
    public function errors_input_required()
    {
        $input = [];
        $response = $this->apiPost($this->endpoint, $input);
        $this->responseWithError($response, 'users.register.fr.name');
        $this->responseWithError($response, 'users.register.fr.email');
        $this->responseWithError($response, 'users.register.fr.password');
    }
    
}
