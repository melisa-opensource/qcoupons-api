<?php

use Illuminate\Foundation\Inspiring;
use App\Models\Coupons;
use App\Models\Users;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');
Artisan::command('coupons', function () {
    $this->comment(Coupons::select('code')->get()->toJson());
})->describe('Coupons');
Artisan::command('users', function () {
    $this->comment(Users::select('email')->get()->toJson());
})->describe('Users');
