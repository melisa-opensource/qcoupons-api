<?php

Route::group([
    'prefix'=>'v1',
    'namespace'=>'v1'
], function () {
    Route::post('login', 'ProxyController@login');
    Route::post('refresh-token', 'ProxyController@refreshToken');
    Route::post('register', 'UsersController@register');
    Route::group([
        'prefix'=>'products'
    ], function () {
        Route::get('/', 'ProductsController@paging');
        Route::get('{id}', 'ProductsController@report');
        Route::post('/', 'ProductsController@create')
            ->middleware('auth:api');
    });
    Route::group([
        'prefix'=>'coupons'
    ], function () {
        Route::get('/', 'CouponsController@paging')
            ->middleware('auth:api');
    });
    Route::group([
        'prefix'=>'shopping'
    ], function () {
        Route::post('/', 'ShoppingController@create')
            ->middleware('auth:api');
        Route::get('/', 'ShoppingController@paging')
            ->middleware('auth:api');
    });
    Route::group([
        'prefix'=>'users'
    ], function () {
        Route::group([
            'prefix'=>'coupons',
            'namespace'=>'Users'
        ], function () {
            Route::post('/', 'CouponsController@add')
                ->middleware('auth:api');
        });
        Route::get('ranking', 'UsersController@ranking')
            ->middleware('auth:api');
        Route::get('profile', 'UsersController@profile')
            ->middleware('auth:api');
        Route::get('{id}', 'UsersController@report')
            ->middleware('auth:api');
    });
});
