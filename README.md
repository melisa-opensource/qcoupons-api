# Seeders

1. Auto create users
- User admin is created with the email indicated in the environment variable USER_DEVELOPER_EMAIL
- The default admin password is developer
- You can customize the password by specifying the environment variable USER_DEVELOPER_PASSWORD
1. Auto create coupons
1. Auto will generate coupons and products

# Installation

1. Create .env file
```bash
cp .env-example .env
```
2. Create database
```sql
CREATE SCHEMA IF NOT EXISTS `qcoupons` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
```
3. Install packages, migrate database, run seeders and install passport

```bash
composer install
php artisan migrate:refresh
php artisan db:seed
php artisan passport:install
```

# Run tests

```bash
phpunit --config phpunit.xml 
phpunit --config phpunit.xml --group completed 
phpunit --config phpunit.xml --group dev 
phpunit --config phpunit.xml --group products 
phpunit --config phpunit.xml --group coupons 
phpunit --config phpunit.xml --group users 
```

## Posible problems with test

```bash
# run and by happy
composer dumpautoload
```

## Resources

1. [API Postman](https://www.getpostman.com/collections/e06598ec8a9da1f4e3c1)
1. [Environment with Laradock](https://laradock.io/)
